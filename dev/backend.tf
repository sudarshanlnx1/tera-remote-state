terraform {
  backend "s3" {
    bucket         = "13-sat-remote"
    key            = "dev/terraform.tfstate"
    region         = "us-west-2"
    encrypt        = true
    dynamodb_table = "13-dyno-tera"

  }
}